package com.wordpress.haelnote.resttemplatessl.base;

public interface Accessor {

	public String get() throws Exception;
	
}
