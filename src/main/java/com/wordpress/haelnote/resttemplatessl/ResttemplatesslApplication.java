package com.wordpress.haelnote.resttemplatessl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResttemplatesslApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResttemplatesslApplication.class, args);
	}

}
