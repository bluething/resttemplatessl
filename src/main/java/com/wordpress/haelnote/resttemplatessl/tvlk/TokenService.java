package com.wordpress.haelnote.resttemplatessl.tvlk;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.wordpress.haelnote.resttemplatessl.base.Accessor;

@Service("tokenService")
public class TokenService implements Accessor{

	private RestTemplate restTemplate;

	@Autowired
	public TokenService(RestTemplateBuilder builder) {
		restTemplate = builder.build();
	}

	@Override
	public String get() throws Exception {
		String response = "";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
			map.add("client_id", "dummyclient");
			map.add("client_secret", "dummysecret");

			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

			ResponseEntity<String> responseEntity = restTemplate.postForEntity(
					"https://api.afc.traveloka.com/oauth/accesstoken", request , String.class );
			response = responseEntity.getBody();
		} catch(HttpClientErrorException hcex) {
			System.out.println(hcex);
		}
		return response;
	}

}
