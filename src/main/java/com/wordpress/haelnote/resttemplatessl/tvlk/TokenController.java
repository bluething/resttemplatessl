package com.wordpress.haelnote.resttemplatessl.tvlk;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wordpress.haelnote.resttemplatessl.base.Accessor;

@RestController
public class TokenController {
	
	private Accessor accessor;
	
	@Autowired
	public TokenController(@Qualifier("tokenService") Accessor accessor) {
		this.accessor = accessor;
	}
	
	@GetMapping("/token")
	public String getAccessToken() throws Exception {
		return accessor.get();
	}
	
}
