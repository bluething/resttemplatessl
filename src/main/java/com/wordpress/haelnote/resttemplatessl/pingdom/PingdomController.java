package com.wordpress.haelnote.resttemplatessl.pingdom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wordpress.haelnote.resttemplatessl.base.Accessor;

@RestController
public class PingdomController {

	private Accessor accessor;
	
	@Autowired
	public PingdomController(@Qualifier("pingDomService") Accessor accessor) {
		this.accessor = accessor;
	}
	
	@GetMapping("/pingdom")
	public String checkUrl() throws Exception {
		return accessor.get();
	}
	
}
