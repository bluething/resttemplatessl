package com.wordpress.haelnote.resttemplatessl.pingdom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.wordpress.haelnote.resttemplatessl.base.Accessor;

@Service("pingDomService")
public class PingDomService implements Accessor {

	private RestTemplate restTemplate;

	@Autowired
	public PingDomService(RestTemplateBuilder builder) {
		restTemplate = builder.build();
	}

	@Override
	public String get() throws Exception {
		String response = "";
		try {
			ResponseEntity<String> responseEntity = restTemplate.exchange(
					"https://valid-isrgrootx1.letsencrypt.org/", HttpMethod.GET, null, String.class);
			response = responseEntity.getBody();
		} catch(HttpClientErrorException hcex) {
			System.out.println(hcex.getMessage());
		}
		return response;
	}

}
