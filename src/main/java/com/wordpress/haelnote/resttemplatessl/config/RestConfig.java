package com.wordpress.haelnote.resttemplatessl.config;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import com.fasterxml.jackson.databind.SerializationFeature;

@Configuration
@ConfigurationProperties(prefix="restconfig")
public class RestConfig {

	@Bean
	public ClientHttpRequestFactory requestFactory(
			@Value("${restconfig.maxconn}") Integer maxconn,
			@Value("${restconfig.maxconnperroute}") Integer maxconnperroute,
			@Value("${restconfig.sockettimeout}") Integer sockettimeout,
			@Value("${restconfig.readtimeout}") Integer readtimeout,
			@Value("${restconfig.pooltimeout}") Integer pooltimeout,
			@Value("${restconfig.keepalive}") Integer keepalive
			) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		
		SSLContextBuilder builder = new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy());
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());

		Registry<ConnectionSocketFactory> socketFactoryRegistry = 
				RegistryBuilder.<ConnectionSocketFactory> create()
				.register("https", sslsf)
				.register("http", new PlainConnectionSocketFactory())
				.build();

		PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
		connectionManager.setMaxTotal(maxconn);
		connectionManager.setDefaultMaxPerRoute(maxconnperroute);
		connectionManager.setValidateAfterInactivity(5000);
		
		RequestConfig requestConfig = RequestConfig
				.custom()
				.setConnectionRequestTimeout(pooltimeout)
				.setSocketTimeout(readtimeout)
				.setConnectTimeout(sockettimeout)
				.build();
		
		CloseableHttpClient httpClient = HttpClients
				.custom().setSSLHostnameVerifier(new NoopHostnameVerifier())
				.setConnectionManager(connectionManager)
				.setKeepAliveStrategy((httpResponse, httpContext) -> keepalive)
				.setDefaultRequestConfig(requestConfig)
				.build();
		return new HttpComponentsClientHttpRequestFactory(httpClient);
	}

	@Bean
	public RestTemplateBuilder restTemplateBuilder(ClientHttpRequestFactory requestFactory) {

		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();

		MappingJackson2HttpMessageConverter jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
		jsonHttpMessageConverter.getObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

		messageConverters.add(new ByteArrayHttpMessageConverter());
		messageConverters.add(new StringHttpMessageConverter());
		messageConverters.add(new ResourceHttpMessageConverter(false));
		messageConverters.add(jsonHttpMessageConverter);
		messageConverters.add(new FormHttpMessageConverter());

		return new RestTemplateBuilder(restTemplate -> restTemplate.setRequestFactory(requestFactory)).messageConverters(messageConverters);
	}

}
