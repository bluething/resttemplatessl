package com.wordpress.haelnote.resttemplatessl.test.integration.tvlk;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest
@AutoConfigureMockMvc
public class DummyAccessTokenIT {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void getAccessTokenSuccessCall() throws Exception {
		mockMvc.perform(get("/token")).andExpect(status().isOk());
	}
	
	@Test
	public void getAccessTokenSuccessCallReturnTokenString() throws Exception {
		MvcResult result =  mockMvc.perform(get("/token")).andReturn();	
		
		assertEquals("", result.getResponse().getContentAsString());
	}
}
