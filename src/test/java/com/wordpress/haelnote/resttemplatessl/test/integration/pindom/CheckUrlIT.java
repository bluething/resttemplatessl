package com.wordpress.haelnote.resttemplatessl.test.integration.pindom;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class CheckUrlIT {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void checkUrlReturnSuccess() throws Exception {
		mockMvc.perform(get("/pingdom")).andExpect(status().isOk());
	}

}
